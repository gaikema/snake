/// <reference path="node_modules/@types/randomcolor/index.d.ts" />
import randomColor = require('randomcolor');

export class Snake
{
	// How many pixels long each segment of the snake is.
	private static l: number = 10;
	
	private body: Array<[number, number]>;
	private canvas: HTMLCanvasElement;
	private ctx: CanvasRenderingContext2D;

	constructor(canvas: HTMLCanvasElement)
	{
		this.canvas = canvas;
		this.ctx = canvas.getContext('2d');
		
		let x = 0;
		let y = 0;
		this.body = [
			[x, y],
			[x, y + Snake.l],
			[x, y + 2*Snake.l],
			[x, y + 3*Snake.l],
			[x, y + 4*Snake.l]
		];
		// Lol I put them in backwards.
		this.body.reverse();

		this.draw();
	}

	private draw(): void
	{
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

		// Draw every square in the body.
		this.ctx.beginPath();
		for (let square of this.body)
		{
			this.wrap(square);
			this.ctx.fillStyle = randomColor({
				hue: 'green',
				luminosity: 'bright'
			});
			this.ctx.strokeStyle = 'black';
			this.ctx.fillRect(square[0], square[1], Snake.l, Snake.l);
			this.ctx.rect(square[0], square[1], Snake.l, Snake.l);
		}
		this.ctx.stroke();
	}

	public move(dx: number, dy: number): void
	{
		// Moves by taking the last square in the body and moving it to the front.
		this.body.pop();
		let first: [number, number] = [this.body[0][0] + dx*Snake.l, this.body[0][1] + dy*Snake.l];
		this.body.unshift(first);

		this.draw();
	}

	public grow(): void
	{
		let last = this.body[this.body.length-1];
		this.body.push([last[0] - Snake.l, last[1] - Snake.l]);

		this.draw();
	}

	public getLength(): number
	{
		return this.body.length;
	}

	public getHead(): [number, number]
	{
		return this.body[0];
	}

	public checkCollisions(): boolean
	{
		let head = this.body[0];
		for (let i = 2; i < this.body.length; i++)
		{
			let current = this.body[i];
			if (head[0] == current[0] && head[1] == current[1])
				return true;
		}
		return false;
	}

	private wrap(p: [number, number]): void
	{
		// Wrap x-coordinate.
		if (p[0] < 0)
			p[0] = this.canvas.width;
		else 
			p[0] = p[0] % this.canvas.width;
		
		// Wrap y-coordinate.
		if (p[1] < 0)
			p[1] = this.canvas.width;
		else 
			p[1] = p[1] % this.canvas.width;
	}
}