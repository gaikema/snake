/// <reference path="node_modules/@types/randomcolor/index.d.ts" />
import randomColor = require('randomcolor');

export class Food 
{
	private ctx: CanvasRenderingContext2D;
	// Top left corner.
	private pt: [number, number];
	private length: number;
	private color: string;

	constructor(pt: [number, number], length: number, ctx: CanvasRenderingContext2D)
	{
		this.pt = pt;
		this.length = length;
		this.ctx = ctx;
		this.color = randomColor();
	}

	public draw(): void
	{
		//this.ctx.beginPath();
		this.ctx.fillStyle = this.color;
		this.ctx.strokeStyle = 'black';
		this.ctx.fillRect(this.pt[0], this.pt[1], this.length, this.length);
		this.ctx.rect(this.pt[0], this.pt[1], this.length, this.length);
		this.ctx.stroke();
	}

	public getPosition(): [number, number]
	{
		return this.pt;
	}
}