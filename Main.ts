import {Game} from "./Game";

let canvas = <HTMLCanvasElement>document.getElementById("myCanvas");

let game = new Game(canvas);
game.run();