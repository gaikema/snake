// https://www.dropbox.com/sh/ffiul9uh8jo9x90/AAD9xNmP9TXLT1FbochK9MiLa?dl=0

var path = require('path');
var webpack = require("webpack"); 

module.exports = {
	entry: "./Main.ts",
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname, "dist")
	},
 
	resolve: {
		// Add '.ts' and '.tsx' as resolvable extensions.
		extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".json"]
	},
 
	module: {
		loaders: [
			// All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
			{ test: /\.tsx?$/, loader: "ts-loader" }
		],
 
		preLoaders: [
			// All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
			{ test: /\.js$/, loader: "source-map-loader" }
		]
	},
	plugins: [
    	new webpack.optimize.UglifyJsPlugin({minimize: true})
  	]
};