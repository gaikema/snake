# This script installs the dev-dependencies, compiles the code, 
# and then deletes the node_modules folder, because it is so big.
# Run only before release, because this script can take a while.

yarn install
webpack
rm -r node_modules