import {Snake} from "./Snake";
import {Food} from "./Food";

export class Game
{
	private static l: number = 10;
	
	private snake: Snake;
	private canvas: HTMLCanvasElement;
	private ctx: CanvasRenderingContext2D;
	private direction: [number, number];
	private foodEaten: boolean;
	private currentFood: Food;
	
	constructor(canvas: HTMLCanvasElement)
	{
		this.canvas = canvas;
		this.ctx = canvas.getContext('2d');
		this.snake = new Snake(canvas);
		this.direction = [0, 1];
		this.foodEaten = true;
	}

	public run(): void 
	{
		let _this = this;

		// Get direction.
		document.onkeydown = function(event) {
			let key: number = event.which;

			// Up
			if (key == 38 && _this.direction[1] != 1)
				_this.direction = [0, -1];
			// Down
			if (key == 40 && _this.direction[1] != -1)
				_this.direction = [0, 1];
			// Right
			if (key == 39 && _this.direction[0] != -1)
				_this.direction = [1, 0];
			// Left
			if (key == 37 && _this.direction[0] != 1)
				_this.direction = [-1, 0];
		}

		// Game loop.
		setInterval(function(){
			_this.addFood();
			_this.eatFood();
			_this.snake.move(_this.direction[0], _this.direction[1]);
			_this.currentFood.draw();
			_this.checkGameOver();
			_this.updateScore();
		}, 100);
	}

	private addFood(): void
	{
		if (this.foodEaten)
		{
			let x = Math.floor(Math.random() * this.canvas.width);
			let y = Math.floor(Math.random() * this.canvas.height);
			this.currentFood = new Food([x, y], Game.l, this.ctx);
			this.foodEaten = false;
		}
	}

	private eatFood(): void
	{
		let snakeHead = this.snake.getHead();
		let foodHead = this.currentFood.getPosition();
		if (Math.abs(snakeHead[0]-foodHead[0]) <= Game.l && Math.abs(snakeHead[1]-foodHead[1]) <= Game.l)
		{
			this.snake.grow();
			this.foodEaten = true;
		}
	}

	/**
	 * Update the score.
	 */
	private updateScore(): void
	{
		this.ctx.fillStyle = 'black';
		this.ctx.font = "20px Arial";
		let score = "Score: " + this.snake.getLength();
		this.ctx.fillText(score, 450, 30);
	}

	private checkGameOver(): void
	{
		if (this.snake.checkCollisions())
		{
			let endString = "Game over!\nYour score: " + this.snake.getLength() + ".";
			alert(endString);
			location.reload();
		}
	}
}